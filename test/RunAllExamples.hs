{-| Module  : FiniteCategoriesGraphViz
Description : Run all examples of the project.
Copyright   : Guillaume Sabbagh 2022
License     : GPL-3
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Run all examples of the project. See results in the folder OutputGraphViz.
-}

module Main
(
    main
)
where
    import Math.FiniteCategories.CompositionGraph
    import Math.IO.FiniteCategories.ExportGraphViz
    
    -- | Run all examples of the project. See results in the folder OutputGraphViz.
    main = do
        let Right cg = readCGString "A -f-> B\nB -g-> C\nC -h-> B\nB -g-> C -h-> B = <ID>\nC -h-> B -g-> C -h-> B -g-> C = C -h-> B -g-> C"
        catToPdf cg "OutputGraphViz/CompositionGraph"