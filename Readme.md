# FiniteCategoriesGraphViz

The goal of this project is to represent small finite categories in order to make usual constructions automatically on them (e.g. (co)limits, (co)completion, adjunctions, etc.)

## Table of Contents
1. [General Info](#general-info)
2. [Technologies](#technologies)
3. [Installation](#installation)
4. [Collaboration](#collaboration)
5. [Examples](#examples)

## General Info

This package provides tools to export categories of the FiniteCategories using GraphViz. See the package [FiniteCategories](https://hackage.haskell.org/package/FiniteCategories) on hackage.

## Technologies

The project uses GraphViz for visualizing the categories created.

## Installation

To use the graphviz exports, you must first install graphviz (see [graphviz website](https://graphviz.org/download/)) and make sure that Graphviz folder is in the path (dot should be a callable program from your terminal, if you are on Windows see [this tutorial](https://stackoverflow.com/questions/44272416/how-to-add-a-folder-to-path-environment-variable-in-windows-10-with-screensho#44272417) and if you are on unix see [this tutorial]( https://unix.stackexchange.com/questions/26047/how-to-correctly-add-a-path-to-path)).

Then you can call the functions `catToPdf`, `diagToPdf`, `diag2ToPdf`, `natToPdf`.

## Collaboration

All contributions are appreciated! Contact me by email for any information.

## Usage

To run all examples of the project, clone the repository and run in a terminal from the repository the following command :

```cabal test```

You can then find the graphviz output in the folder `OutputGraphViz/`.


## Examples

A category exported with graphviz looks like the following image : 

![Category](https://gitlab.utc.fr/gsabbagh/FiniteCategories/-/raw/master/Images/ExampleCategory.png)

A diagram on this category selecting two objects C and D is represented next :

![Diagram](https://gitlab.utc.fr/gsabbagh/FiniteCategories/-/raw/master/Images/ExampleDiagram.png)

A cone on this diagram follows, the apex of the cone is in green, its legs are in yellow and the diagram is in blue :

![Cone](https://gitlab.utc.fr/gsabbagh/FiniteCategories/-/raw/master/Images/ExampleCone.png)

The limiting cone is represented below, it is the product of the two objects C and D.

![Limit](https://gitlab.utc.fr/gsabbagh/FiniteCategories/-/raw/master/Images/ExampleLimit.png)
